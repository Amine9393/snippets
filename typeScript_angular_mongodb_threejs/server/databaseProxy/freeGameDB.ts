import { FreeGameDBError } from "../../../../common/communication/freeGameDBError";
import { Record } from "../../../../common/models/record";
import { Scene } from "../../../../common/models/scene";
import { IFreeGame } from "../../database/freeGame/IFreeGame";
import { FREEGAME } from "../../database/freeGame/freeGame.model";
import { Utils } from "../../utils/utils";

export class FreeGameDB {

    public async getAll(): Promise<IFreeGame[] | Error> {
        return new Promise<IFreeGame[] | Error>((
            resolve: (value?: IFreeGame[] | PromiseLike<IFreeGame[]>) => void,
            reject: (reason?: FreeGameDBError) => void) => {
            FREEGAME.find((e: FreeGameDBError, games: IFreeGame[]) => {
                if (e) { reject(e); }
                resolve(games);
            });
        });
    }

    public async create(name: String, thumbnail: String, oScene: Scene, mScene: Scene): Promise<IFreeGame | FreeGameDBError> {
        return new Promise<IFreeGame | FreeGameDBError>((
            resolve: (value?: IFreeGame | PromiseLike<IFreeGame>) => void,
            reject: (reason?: FreeGameDBError) => void) => {
            this.newGame(name, thumbnail, oScene, mScene).save((e: FreeGameDBError, savedGame: IFreeGame) => {
                if (e) {
                    reject(e);
                } else {
                    resolve(savedGame);
                }
            });

        });
    }

    public async findGameWithId(id: String): Promise<IFreeGame | Error> {
        return new Promise<IFreeGame | Error>((
            resolve: (value?: IFreeGame | PromiseLike<IFreeGame>) => void,
            reject: (reason?: Error) => void) => {
                FREEGAME.findOne({gameId: id}, (e: Error, gameFound: IFreeGame) => {
                    if (e) {
                        reject(e);
                    }
                    resolve(gameFound);
                });
        });
    }

    public async delete(id: String): Promise<void | FreeGameDBError> {
        return new Promise<void | FreeGameDBError>((
            resolve: (value?: void | PromiseLike<void>) => void,

            reject: (reason?: FreeGameDBError) => void) => {
            FREEGAME.findOneAndDelete({ gameId: id }, (e: FreeGameDBError, deletedGame: IFreeGame) => {
                if (e) {
                    reject(e);
                }
                resolve();
            });
        });
    }

    public async reset(id: String): Promise<void | FreeGameDBError> {
        return new Promise<void | FreeGameDBError>((
            resolve: (value?: void | PromiseLike<void>) => void,
            reject: (reason?: FreeGameDBError) => void) => {
            const n: number = 3;
            const sTime: Record[] = Utils.generateRandomRecords(n);
            const vsTime: Record[] = Utils.generateRandomRecords(n);

            const update: Object = {
                $set: {
                    soloTime: sTime,
                    vsTime: vsTime,
                },
            };

            FREEGAME.updateOne({ gameId: id }, update, (e: FreeGameDBError) => {
                if (e) {
                    reject(e);
                } else {
                    resolve();

                }
            });
        });
    }

    public newGame(name: String, thumbnail: String, oScene: Scene,
                   mScene: Scene, highscoreDimensions: number = 3): IFreeGame {

        const soloT: Record[] = Utils.generateRandomRecords(highscoreDimensions);
        const vsT: Record[] = Utils.generateRandomRecords(highscoreDimensions);

        return new FREEGAME({
            gameId: Utils.uniqueId(),
            name: name,
            thumbnail: thumbnail,
            originalScene: oScene,
            modifiedScene: mScene,
            soloTime: soloT,
            vsTime: vsT,
        });
    }

}
