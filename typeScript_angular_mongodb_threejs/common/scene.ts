
export enum SceneObjectType {
    BOX,
    SPHERE,
    CYLINDER,
    CONE,
    PYRAMID,
}

export enum SceneModification {
    ADD,
    REMOVE,
    CHANGE,
}

export class Point3D {
    public x: number;
    public y: number;
    public z: number;

    public constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

export enum ObjectColor {
    RED = 0xFF0000,
    GREEN = 0x00FF00,
    BLUE = 0x0000FF,
    YELLOW = 0xFFFF00,
    CYAN = 0x00FFFF,
    WHITE = 0xFFFFFF,
}

export class SceneObject {
    public id: string;
    public type: SceneObjectType;
    public w: number;
    public h: number;
    public d: number;
    public r: number;
    public hexColor: number;
    public position: Point3D;
    public rotation: Point3D;

    public copy(object: SceneObject): void{
        this.id = object.id;
        this.type = object.type;
        this.w = object.w;
        this.h = object.h;
        this.d = object.d;
        this.r = object.r;
        this.hexColor = object.hexColor;
        this.position = new Point3D(object.position.x, object.position.y, object.position.z);
        this.rotation = new Point3D(object.rotation.x, object.rotation.y, object.rotation.z);
    }
}

export class Scene {
    public backgroundColor: number;
    public objects: SceneObject[] = [];

    public copy(scene: Scene){
        this.backgroundColor = scene.backgroundColor;
        scene.objects.forEach((originalObject: SceneObject, index: number) => {
            this.objects[index] = new SceneObject();
            this.objects[index].copy(originalObject);
        });
    }
}