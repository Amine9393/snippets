import { SceneGenerationStatus as SGStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { Scene, SceneModification, SceneObject } from "../../../../common/models/scene";
import { Pair } from "../../../../common/models/types";
import { Utils } from "../../utils/utils";
import { SceneModifier } from "./sceneModifier";
import { SceneObjectGenerator } from "./sceneObjectGenerator";

const MIN_SCENE_OBJECTS: number = 10;
const MAX_SCENE_OBJECTS: number = 200;
export class SceneGenerator {
    private originalScene: Scene = new Scene();
    private modifiedScene: Scene = new Scene();
    private objectsCount: number = 0;
    private sceneObjectGen: SceneObjectGenerator;
    private sceneModifier: SceneModifier;

    public generateScenes(objectsCount: number, checkBoxes: boolean[]): ServerResponse<SGStatus, Pair<Scene>> {
        if (objectsCount < MIN_SCENE_OBJECTS || objectsCount > MAX_SCENE_OBJECTS) {
            return {
                status: SGStatus.E_INVALID_OBJECT_COUNT,
                response: null,
            };
        }

        this.objectsCount = objectsCount;
        this.sceneObjectGen = new SceneObjectGenerator(objectsCount);
        this.sceneModifier = new SceneModifier(this.convertToModifications(checkBoxes));

        this.generateOriginalScene();
        this.modifiedScene = this.sceneModifier.modify(this.originalScene);

        return {
            status: SGStatus.S_OK,
            response: {first: this.originalScene, second: this.modifiedScene},
        };
    }

    private convertToModifications(checkBoxes: boolean[]): SceneModification[] {
        const modification: SceneModification[] = [];
        checkBoxes.forEach((value: boolean, index: number) => {
            if (value) {
                modification.push(index);
            }
        });

        return modification;
    }

    private generateOriginalScene(): void {
        this.originalScene.backgroundColor = SceneObjectGenerator.COLORS[Utils.rand(0, SceneObjectGenerator.COLORS.length - 1)];

        for (let i: number = 0; i < this.objectsCount; i++) {
            const sceneObject: SceneObject = this.sceneObjectGen.randomSceneObject(this.originalScene);
            this.originalScene.objects.push(sceneObject);
        }
    }
}
