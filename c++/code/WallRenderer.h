/*
 * Nom: WallRenderer.h
 * Auteur: Sébastien Zerbato- équipe 23-28
 * Date: 13 novembre 2018
 */
#include "wallSensorManager.h"
#include "wallDisplay.h"
#include "LCD.h"
#include <math.h>
#include <stdlib.h>
#include <avr/io.h>   

class WallRenderer
{

public:
    WallRenderer();
    Point calculateDistances(); 
    Point acq(bool dir);
    void print();
    void debugPrint();

private:
    //Private Methods
    uint8_t quickMedian(uint8_t *uncertainValues);
    void centerFromRight(Point& data);
    void centerFromLeft(Point& data);
    void initRendering(Point& t);
    float pythagore(float a, float o);

    //Constants
    //Distance [cm] du parcours vers la ligne jaune
    static const uint8_t DISTANCE_CORRECTION=0x07;
    //Index du pivot
    static const uint8_t MIDDLE_INDEX = 5;
    //Nombre donnees lues par iteration, ici suppose 10
    static const uint8_t LAST_INDEX = 10;         
    // Size of recorded distances
    static const unsigned int NUMBER_OF_MEDIAN_OBSERVATION = 400;
    // Total distance while capturing wall data  
    static const uint8_t TOTAL_DISTANCE = 122;
    const uint8_t ACQ_PERIOD = 12;
	unsigned int counter = 0; 

    //Attributes
    Point distances[NUMBER_OF_MEDIAN_OBSERVATION];
    Point wallLength;
    uint8_t valuesL_[LAST_INDEX + 1];
    uint8_t valuesR_[LAST_INDEX + 1];
    uint16_t distancesCurrentIndex=0x00;
    int acqCounter_;

    //Dependencies Attributes
    WallSensorManager wsm_;
    WallDisplay wd_;
    LCD lcd_;
};
