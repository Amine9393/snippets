import { Component, Input, OnInit } from "@angular/core";
import { FreeGame } from "../../../../common/models/freeGame";

@Component({
  selector: "app-free-game-view",
  templateUrl: "./free-game-view.component.html",
  styleUrls: ["./free-game-view.component.css"],
})
export class FreeGameViewComponent implements OnInit {
  @Input() public game: FreeGame;

  public differencesCount: number;

  public ngOnInit(): void {
    this.differencesCount++;
    console.log(this.game);
  }
}
