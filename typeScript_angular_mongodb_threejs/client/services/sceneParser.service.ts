import { Injectable } from "@angular/core";
import * as THREE from "three";
import { Point3D, Scene, SceneObject, SceneObjectType } from "../../../../common/models/scene";

export type ParsedScene = [THREE.Scene, THREE.Camera];

@Injectable()
export class SceneParserService {
    private scene: THREE.Scene;
    private camera: THREE.Camera;

    public constructor(loadedScene: Scene, w: number = window.innerWidth, h: number = window.innerHeight) {
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(loadedScene.backgroundColor);
        const fov: number = 75;
        const far: number = 10000;
        const xPosition: number = -100;
        const yPosition: number = -100;
        const zPosition: number = 10;
        this.camera = new THREE.PerspectiveCamera(fov, w / h, 1, far);
        this.camera.position.x = xPosition;
        this.camera.position.y = yPosition;
        this.camera.position.z = zPosition;

        loadedScene.objects.forEach((object: SceneObject) => this.generateGeometry(object));
    }

    public getParsedScene(): ParsedScene {
        return [this.scene, this.camera];
    }

    public getThumbnail(): Uint8Array {
        const renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer({preserveDrawingBuffer: true});
        const width: number = 640;
        const height: number = 480;
        renderer.setSize(width, height);
        renderer.render(this.scene, this.camera);
        const dataUrl: string = renderer.domElement.toDataURL();

        return this.toArrayBuffer(dataUrl);
    }

    private toArrayBuffer(dataUrl: string): Uint8Array {
        const BASE64_MARKER: string = ";base64,";
        const base64Index: number = dataUrl.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        const base64: string = dataUrl.substring(base64Index);
        const raw: string = window.atob(base64);
        const rawLength: number = raw.length;
        const array: Uint8Array = new Uint8Array(new ArrayBuffer(rawLength));

        for (let i: number = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }

        return array;
    }

    private generateGeometry(object: SceneObject): void {

        let mesh: THREE.Mesh;
        switch (object.type) {
            case SceneObjectType.BOX:
                mesh = this.addBox(object.w, object.h, object.d, object.hexColor);
                break;

            case SceneObjectType.SPHERE:
                mesh = this.addSphere(object.r, object.hexColor);
                break;

            case SceneObjectType.CYLINDER:
                mesh = this.addCylinder(object.r, object.h, object.hexColor);
                break;

            case SceneObjectType.CONE:
                mesh = this.addCone(object.r, object.h, object.hexColor);
                break;

            case SceneObjectType.PYRAMID:
                mesh = this.addPyramid(object.r, object.hexColor);
                break;

            default:
                return;
        }

        this.addMesh(mesh, object.position, object.rotation);
    }

    private addBox(w: number, h: number, d: number, hexColor: number): THREE.Mesh {
        const geometry: THREE.BoxGeometry = new THREE.BoxGeometry(w, h, d);
        const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: hexColor, wireframe: true});

        return new THREE.Mesh(geometry, material);
    }

    private addSphere(r: number, hexColor: number): THREE.Mesh {
        const geometry: THREE.SphereGeometry = new THREE.SphereGeometry(r);
        const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: hexColor, wireframe: true});

        return new THREE.Mesh(geometry, material);
    }

    private addCylinder(r: number, h: number, hexColor: number): THREE.Mesh {
        const geometry: THREE.CylinderGeometry = new THREE.CylinderGeometry(r, r, h);
        const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: hexColor, wireframe: true});

        return new THREE.Mesh(geometry, material);
    }

    private addCone(r: number, h: number, hexColor: number): THREE.Mesh {
        const geometry: THREE.ConeGeometry = new THREE.ConeGeometry(r, h);
        const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: hexColor, wireframe: true});

        return new THREE.Mesh(geometry, material);
    }

    private addPyramid(r: number, hexColor: number): THREE.Mesh {
        const geometry: THREE.TetrahedronGeometry = new THREE.TetrahedronGeometry(r);
        const material: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: hexColor, wireframe: true});

        return new THREE.Mesh(geometry, material);
    }

    private addMesh(mesh: THREE.Mesh, position: Point3D, rotation: Point3D): void {
        mesh.position.set(position.x, position.y, position.z);
        mesh.rotation.set(rotation.x, rotation.y, rotation.z);
        this.scene.add(mesh);
    }

}
