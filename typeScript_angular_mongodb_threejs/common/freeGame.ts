import { Record } from "./record";
import { Scene } from "./scene";

export interface FreeGame {
    gameId: String;
    name: String;
    thumbnail: String;
    originalScene: Scene;
    modifiedScene: Scene;
    soloTime: Record[];
    vsTime: Record[];
}