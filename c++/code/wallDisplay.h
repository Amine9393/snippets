/* Fichier: disntanceDisplayer.h
 * Auteur: Christopher Sigouin-Bond, équipe 23-28
 * Date: 20 novembre 2018
 */

#ifndef WALL_DISPLAY_H
#define WALL_DISPLAY_H

#include "Communication.h"
#include "Light.h"

struct Point
{
	uint8_t l,r;
	Point(){ l=0; r=0; };
};

class WallDisplay
{
	public:
		WallDisplay();
		void convertDataTable(Point dataToConvert[], uint8_t& dataNumber);
		void displayWalls(const Point data[], int size);
		void displayDebugWalls(const Point data[], int size);
		
	private:
		static const uint8_t LINES_TO_WRITE = 96;
		static const uint8_t CHARACTERS_HALF = 95;
		static const uint8_t TUNNEL_HALF_WIDTH = 61;
		
		void printLine(uint8_t l, uint8_t r);
		void printFirstHalfOfLine(const uint8_t& leftDistance);
		void printLastHalfOfLine(const uint8_t& rightDistance);
};
#endif
