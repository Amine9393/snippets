/********************************************
* Titre: classe light.h
* Date premiere version : 22 octobre 2018
* Auteur: Samuel Charbonneau
* Utilité: Afficher une couleur sur un del
*******************************************/

#ifndef LIGHT_H
#define LIGHT_H

#define F_CPU 8000000UL
#define MAX_DEL 1

#include <avr/io.h>
#include <util/delay.h>
#include "PortControl.h"

enum class Colors {RED, GREEN, AMBER, NONE};

typedef struct
{
	char port;
	uint8_t pin1;
	uint8_t pin2;
}
Del;

class Light 
{
public:
	static void setLight(Colors color);
	static void blink(uint8_t times, Colors color1, Colors color2 = Colors::NONE);
	static void toggle(Colors color1, Colors color2 = Colors::NONE);
	static Colors getCurrentColor();

private:
	static constexpr Del DEL = { .port = 'B', .pin1 = 0, .pin2 = 1 };
};
#endif