import { AfterViewInit, Component, ElementRef, Input, ViewChild } from "@angular/core";
import { Scene } from "../../../../common/models/scene";
import { ParsedScene, SceneParserService } from "../services/sceneParser.service";

import * as THREE from "three";

@Component({
  selector: "app-free-view-image",
  templateUrl: "./free-view-image.component.html",
  styleUrls: ["./free-view-image.component.css"],
})
export class FreeViewImageComponent implements AfterViewInit {
  @ViewChild("myCanvas") public canvas: ElementRef;
  @Input() public scene: Scene;
  public renderer: THREE.WebGLRenderer;
  private sceneParser: SceneParserService;
  private parsedScene: ParsedScene;

  public constructor() {
    this.renderer = new THREE.WebGLRenderer();
  }

  public ngAfterViewInit(): void {
    this.sceneParser = new SceneParserService(this.scene, 512, 384);
    this.parsedScene = this.sceneParser.getParsedScene();
    this.renderer.setSize(512, 384);
    this.canvas.nativeElement.appendChild(this.renderer.domElement);
    this.renderer.render(this.parsedScene[0], this.parsedScene[1]);
    // this.animate();
  }

  public animate(): void {
    window.requestAnimationFrame(() => this.animate());
    this.parsedScene = this.sceneParser.getParsedScene();
    this.renderer.render(this.parsedScene[0], this.parsedScene[1]);
  }
}
