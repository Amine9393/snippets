import { Scene, SceneModification, SceneObject } from "../../../../common/models/scene";
import { Utils } from "../../utils/utils";
import { SceneObjectGenerator } from "./sceneObjectGenerator";

export class SceneModifier {
    public readonly DIFFERENCES_COUNT: number = 7;
    private allowedModifications: SceneModification[];
    private modifiedScene: Scene;
    private sceneObjectGenerator: SceneObjectGenerator;
    private nonModifiedObjectsChecksum: string[];
    private modificationCount: number;

    public constructor(allowedModifications: SceneModification[]) {
        this.allowedModifications = [];
        this.allowedModifications = allowedModifications;
        this.nonModifiedObjectsChecksum = [];
        this.modificationCount = 0;
    }

    public modify(originalScene: Scene): Scene {
        this.copy(originalScene);
        this.sceneObjectGenerator = new SceneObjectGenerator(this.modifiedScene.objects.length);

        const REMOVE_ALLOWED: boolean = this.allowedModifications.findIndex((modification: SceneModification) =>
            modification === SceneModification.REMOVE) !== -1;
        const CHANGE_ALLOWED: boolean = this.allowedModifications.findIndex((modification: SceneModification) =>
            modification === SceneModification.CHANGE) !== -1;

        this.modificationCount = 0;

        if ((REMOVE_ALLOWED || CHANGE_ALLOWED) && this.DIFFERENCES_COUNT > originalScene.objects.length) {
            throw new Error("There is more differences than original objects");
        } else {
            while (this.modificationCount < this.DIFFERENCES_COUNT) {
                this.addModification();
            }
        }

        this.modificationCount = 0;

        return this.modifiedScene;
    }

    private copy(originalScene: Scene): void {
        this.modifiedScene = new Scene();
        this.modifiedScene.copy(originalScene);

        originalScene.objects.forEach((object: SceneObject) => {
            this.nonModifiedObjectsChecksum.push(Utils.getChecksum(object));
        });
    }

    private addModification(): void {
        const CURRENT_MODIFICATION: SceneModification = this.allowedModifications[Utils.rand(0, this.allowedModifications.length - 1)];

        switch (CURRENT_MODIFICATION) {
            case SceneModification.ADD:
                this.add();
                break;
            case SceneModification.REMOVE:
                this.remove();
                break;
            case SceneModification.CHANGE:
                this.change();
                break;
            default:
                throw new Error("Invalid SceneModification input");
        }

        this.modificationCount++;
    }

    private add(): void {
        const SCENE_OBJECT: SceneObject = this.sceneObjectGenerator.randomSceneObject(this.modifiedScene);
        this.modifiedScene.objects.push(SCENE_OBJECT);
    }

    private remove(): void {
        const REMOVE_CHECKSUM_IDX: number = Utils.rand(0, this.nonModifiedObjectsChecksum.length - 1);
        const REMOVE_CHECKSUM: string = this.nonModifiedObjectsChecksum[REMOVE_CHECKSUM_IDX];

        this.modifiedScene.objects.forEach((object: SceneObject, index: number) => {
            if (Utils.getChecksum(object) === REMOVE_CHECKSUM) {
                this.modifiedScene.objects.splice(index, 1);
            }
        });

        this.nonModifiedObjectsChecksum.splice(REMOVE_CHECKSUM_IDX, 1);
    }

    private change(): void {
        const CHANGE_CHECKSUM_IDX: number = Utils.rand(0, this.nonModifiedObjectsChecksum.length - 1);
        const CHANGE_CHECKSUM: string = this.nonModifiedObjectsChecksum[CHANGE_CHECKSUM_IDX];
        let randomColor: number = SceneObjectGenerator.COLORS[Utils.rand(0, SceneObjectGenerator.COLORS.length - 1)];

        const CHANGE_IDX: number = this.modifiedScene.objects.findIndex((object: SceneObject) =>
            Utils.getChecksum(object) === CHANGE_CHECKSUM);
        const CURRENT_COLOR: number = this.modifiedScene.objects[CHANGE_IDX].hexColor;

        while (CURRENT_COLOR === randomColor) {
            randomColor = SceneObjectGenerator.COLORS[Utils.rand(0, SceneObjectGenerator.COLORS.length - 1)];
        }

        this.modifiedScene.objects[CHANGE_IDX].hexColor = randomColor;
        this.nonModifiedObjectsChecksum.splice(CHANGE_CHECKSUM_IDX, 1);
    }
}
