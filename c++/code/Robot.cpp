/****************************************************
// Fichier : Robot.cpp
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 30/10/18
// Description: Définition de la classe Robot
*****************************************************/

#include "Robot.h"

volatile bool Robot::WALL_DISPLAY_FINISHED = false;

Robot::Robot()
{
	PortControl::init();
	Communication::init();
	Sound::init();
	acquisitionSide_ = Direction::RIGHT;
	gotoState(RState::INIT);
}

Robot::~Robot()
{
}

void Robot::run()
{
	
	switch(currentState_)
	{
		case RState::INIT:
			init();
			break;

		case RState::AQ:
			aq();
			break;

		case RState::NAQ:
			naq();
			break;

		case RState::ECHO:
			echo();
			break;

		case RState::PRINT:
			print();
			break;

		case RState::END:
			end();
			break;
	};
}

void Robot::init()
{
	Interrupt::waitClicked(INTERRUPT_PORT, INTERRUPT_PIN);
	lcd_.display(".*DiScObAlL*.");	
	Utils::wait(3000);
	gotoState(RState::NAQ);
}

void Robot::aq()
{
	Light::setLight(Colors::RED);
	uint8_t ret = mvt_.run();

	wrd_.acq(acquisitionSide_ == Direction::RIGHT);

	switch(ret)
	{
		case 0: //Turn
			gotoState(RState::NAQ);
			break;

		case 1: //Finished
			gotoState(RState::ECHO);
			break;

		case 2: //Continue
			break;
	}
}

void Robot::naq()
{
	uint8_t ret = mvt_.run();
	switch(ret)
	{
		case 0: //Turn
			gotoState(RState::NAQ);
			break;

		case 1: //Finished
			gotoState(RState::ECHO);
			break;

		case 2: //Continue
			toggleAcqSide();
			gotoState(RState::AQ);
			break;
	}
}

void Robot::echo()
{
	Light::setLight(Colors::NONE);
	lcd_.clear();
	Point d = wrd_.calculateDistances();
	lcd_.displayLR(d.l, d.r);
	gotoState(RState::PRINT);
}

void Robot::print()
{
	Interrupt::waitClicked(INTERRUPT_PORT, INTERRUPT_PIN);
	wrd_.print();
	Light::setLight(Colors::NONE);
	Sound::playSong();
	Light::setLight(Colors::NONE);
	gotoState(RState::END);
}

void Robot::end()
{
	gotoState(RState::PRINT);
}

// CTC, c no prescaler
void Robot::initTimer()
{
	cli();
	DDRD = 0x02;
	TCNT1 = 0;
	TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);
	TIMSK1 = (1 << OCIE1A);
	TCCR1C = 0;
	sei();
}

void Robot::setTimer(uint16_t delay)
{
	OCR1A = delay * 7.8125; // 7.8125 = F_CPU / ( prescaler * 1000 )
	TCNT1 = 0;
}

void Robot::timerFunction()
{
	if(!WALL_DISPLAY_FINISHED)
	{
		Light::toggle(Colors::GREEN);
		setTimer(5000);
	}
}

void Robot::gotoState(RState state)
{
	currentState_ = state;
}

void Robot::toggleAcqSide()
{
	switch(acquisitionSide_)
	{
		case Direction::RIGHT:
			acquisitionSide_ = Direction::LEFT;
		break;

		case Direction::LEFT:
			acquisitionSide_ = Direction::RIGHT;
		break;
	}
}

ISR ( TIMER1_COMPA_vect )
{
	Robot::timerFunction();
}
