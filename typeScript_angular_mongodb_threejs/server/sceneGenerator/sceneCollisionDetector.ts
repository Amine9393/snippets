import { Scene, SceneObject, SceneObjectType } from "../../../../common/models/scene";

export class SceneCollisionDetector {

    public detect(scene: Scene, scObj: SceneObject): boolean {
        return scene.objects.some((curObj: SceneObject) => this.areColliding(scObj, curObj));
    }

    private areColliding(obj1: SceneObject, obj2: SceneObject): boolean {
        const radius1: number = this.boundingBoxDimension(obj1);
        const radius2: number = this.boundingBoxDimension(obj2);

        // Collision in x axis
        let colliding: boolean = this.areCollidingOnAxis(obj1.position.x, obj2.position.x, radius1, radius2);

        // Collision in y axis
        colliding = colliding && this.areCollidingOnAxis(obj1.position.y, obj2.position.y, radius1, radius2);

        // Collision in z axis
        colliding = colliding && this.areCollidingOnAxis(obj1.position.z, obj2.position.z, radius1, radius2);

        return colliding;
    }

    private areCollidingOnAxis(pos1: number, pos2: number, measure1: number, measure2: number): boolean {
        const posL: number = (pos1 > pos2) ? pos2 : pos1;
        const posR: number = (pos1 > pos2) ? pos1 : pos2;
        const measureL: number = (pos1 > pos2) ? measure2 : measure1;
        const measureR: number = (pos1 > pos2) ? measure1 : measure2;

        return (posL + measureL) >= (posR - measureR);
    }

    private boundingBoxDimension(obj: SceneObject): number {
        let measure: number = 0;
        const factor: number = 2;

        switch (obj.type) {
            case SceneObjectType.BOX:
                measure = obj.w;
                break;

            case SceneObjectType.SPHERE:
            case SceneObjectType.PYRAMID:
                measure = obj.r * factor;
                break;

            default:
                const diameter: number = obj.r * factor;
                measure = (diameter > obj.h) ? diameter : obj.h;
                break;
        }

        return Math.round(measure / factor);
    }

}
