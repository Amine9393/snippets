import { Component, OnInit } from "@angular/core";
import { FreeGameService } from "src/app/services/freeGame.service";
import { FreeGame } from "../../../../../common/models/freeGame";
import { SimpleGame } from "../../../../../common/models/simpleGame";
import { SimpleGameService } from "../../services/simpleGame.service";

@Component({
  selector: "app-game-card-admin",
  templateUrl: "./game-card-admin.component.html",
  styleUrls: ["./game-card-admin.component.css"],
})

export class GameCardAdminComponent implements OnInit {
  public constructor(private simpleGameService: SimpleGameService, private freeGameService: FreeGameService) { }

  public readonly title: String = "Liste des jeux";

  public simpleGames: SimpleGame[] = [];
  public freeGames: FreeGame[] = [];

  public deleteSimpleGame(id: string): void {
    this.simpleGameService.deleteGame(id);
  }

  public resetSimpleGame(id: string): void {
    this.simpleGameService.resetGame(id);
  }

  public ngOnInit(): void {
    this.simpleGameService.getAll().subscribe((newSimpleViewGames: SimpleGame[]) => this.simpleGames = newSimpleViewGames);
    this.freeGameService.getAll().subscribe((newFreeViewgames: FreeGame[]) => this.freeGames = newFreeViewgames);
  }

}
