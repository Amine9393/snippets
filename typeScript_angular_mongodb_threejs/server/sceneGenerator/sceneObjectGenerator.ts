import { ObjectColor, Point3D, Scene, SceneObject, SceneObjectType } from "../../../../common/models/scene";
import { Utils } from "../../utils/utils";
import { SceneCollisionDetector } from "./sceneCollisionDetector";

export class SceneObjectGenerator {
    // red, green, blue, yellow, cyan, white
    public static readonly COLORS: number[] = [
        ObjectColor.RED,
        ObjectColor.GREEN,
        ObjectColor.BLUE,
        ObjectColor.YELLOW,
        ObjectColor.CYAN,
        ObjectColor.WHITE,
    ];
    private readonly BOUNDS: number = 1000; // Scene x, y, z bounds
    private readonly PADDING_RATIO: number = 10;
    private objectsCount: number = 1;
    private refSize: number = 0;

    public constructor(objectsCount: number) {
        this.objectsCount = objectsCount;
        this.calculateRefSize();
    }

    public randomSceneObject(scene: Scene): SceneObject {
        const sceneObject: SceneObject = new SceneObject();
        sceneObject.id = Utils.uniqueId();
        const halfFactor: number = 2;

        // Generate random type from enum (SceneObjectType)
        const maxEnum: number = (Object.keys(SceneObjectType).length / halfFactor) - 1;
        sceneObject.type = Utils.rand(0 , maxEnum);
        sceneObject.hexColor = SceneObjectGenerator.COLORS[Utils.rand(0, SceneObjectGenerator.COLORS.length - 1)];

        const sceneCollisionDetector: SceneCollisionDetector = new SceneCollisionDetector();
        do {
            // Calculating random size
            sceneObject.w = this.randomSize();
            sceneObject.h = sceneObject.w;
            sceneObject.d = sceneObject.w;
            sceneObject.r = this.randomSize() / halfFactor;

            // Calculating random position
            sceneObject.position = this.randomPosition();
        } while (sceneCollisionDetector.detect(scene, sceneObject));

        // Calculating random rotation
        sceneObject.rotation = this.randomRotation();

        return sceneObject;
    }

    private randomSize(): number {
        const minRef: number = 0.5; // 50%
        const maxRef: number = 1.5; // 150%

        return Utils.rand(Math.round(this.refSize * minRef), this.refSize * maxRef);
    }

    private randomPosition(): Point3D {
        const halfFactor: number = 0.5;
        const maxRef: number = 1.5;
        const size: number = this.refSize * maxRef;
        const lower: number = Math.round((this.BOUNDS * halfFactor) * -1) + size;
        const upper: number = Math.floor(this.BOUNDS * halfFactor) - size;

        const x: number = Utils.rand(lower, upper);
        const y: number = Utils.rand(lower, upper);
        const z: number = Utils.rand(lower, upper);

        return new Point3D(x, y, z);
    }

    private randomRotation(): Point3D {
        const lower: number = 0;
        const upper: number = 360;

        const x: number = Utils.rand(lower, upper);
        const y: number = Utils.rand(lower, upper);
        const z: number = Utils.rand(lower, upper);

        return new Point3D(x, y, z);
    }

    private calculateRefSize(): void {
        const exponent: number = 3;
        const objectSpace: number = Math.pow(this.BOUNDS, exponent) / this.objectsCount;
        this.refSize = Math.floor(Math.cbrt(objectSpace) / this.PADDING_RATIO);
    }
}
