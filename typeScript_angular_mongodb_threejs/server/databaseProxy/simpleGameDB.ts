import { Record } from "../../../../common/models/record";
import { ISimpleGame } from "../../database/simpleGame/ISimpleGame";
import { SIMPLEGAME } from "../../database/simpleGame/simpleGame.model";
import { Utils } from "../../utils/utils";

export class SimpleGameDB {

    public async getAll(): Promise<ISimpleGame[] | Error> {
        return new Promise<ISimpleGame[] | Error>((
            resolve: (value?: ISimpleGame[] | PromiseLike<ISimpleGame[]>) => void,
            reject: (reason?: Error) => void) => {
                SIMPLEGAME.find((e: Error, games: ISimpleGame[]) => {
                    if (e)  { reject(e); }
                    resolve(games);
                });
        });
    }

    public async create(name: String, oImg: String, mImg: String, dImg: String): Promise<ISimpleGame | Error> {
        return new Promise<ISimpleGame | Error>((
            resolve: (value?: ISimpleGame | PromiseLike<ISimpleGame>) => void,
            reject: (reason?: Error) => void) => {
                const game: ISimpleGame = this.newGame(name, oImg, mImg, dImg);
                game.save((e: Error, savedGame: ISimpleGame) => {
                    if (e) { reject(e); }
                    resolve(savedGame);
                });
        });
    }

    public async findGameWithId(id: String): Promise<ISimpleGame | Error> {
        return new Promise<ISimpleGame | Error>((
            resolve: (value?: ISimpleGame | PromiseLike<ISimpleGame>) => void,
            reject: (reason?: Error) => void) => {
                SIMPLEGAME.findOne({gameId: id}, (e: Error, gameFound: ISimpleGame) => {
                    if (e) {
                        reject(e);
                    }
                    resolve(gameFound);
                });
        });
    }

    public async delete(id: String): Promise<void | Error> {
        return new Promise<void | Error>((
            resolve: (value?: void | PromiseLike<void>) => void,
            reject: (reason?: Error) => void) => {
                SIMPLEGAME.findOneAndDelete({gameId: id}, (e: Error, deletedGame: ISimpleGame) => {
                    if (e) {
                        reject(e);
                    }
                    Utils.removeImageUrl(deletedGame.originalImage.toString());
                    Utils.removeImageUrl(deletedGame.modifiedImage.toString());
                    Utils.removeImageUrl(deletedGame.differencesImage.toString());
                    resolve();
                });
        });
    }

    public async reset(id: String): Promise<void | Error> {
        return new Promise<void | Error>((
            resolve: (value?: void | PromiseLike<void>) => void,
            reject: (reason?: Error) => void) => {
                const n: number = 3;
                const sTime: Record[] = Utils.generateRandomRecords(n);
                const vsTime: Record[] = Utils.generateRandomRecords(n);

                const update: Object = { $set: {
                    soloTime: sTime,
                    vsTime: vsTime,
                }};

                SIMPLEGAME.updateOne({gameId: id}, update, (e: Error) => {
                    if (e) {
                        reject(e);
                    }
                    resolve();
                });
        });
    }

    public newGame(name: String, oImg: String, mImg: String, dImg: String, highscoreDimensions: number = 3): ISimpleGame {

        const soloT: Record[] = Utils.generateRandomRecords(highscoreDimensions);
        const vsT: Record[] = Utils.generateRandomRecords(highscoreDimensions);

        return new SIMPLEGAME({
            gameId: Utils.uniqueId(),
            name: name,
            originalImage: oImg,
            modifiedImage: mImg,
            differencesImage: dImg,
            soloTime: soloT,
            vsTime: vsT,
        });
    }
}
