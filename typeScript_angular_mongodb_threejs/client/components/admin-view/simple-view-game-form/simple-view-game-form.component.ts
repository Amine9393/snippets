import { Component} from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import {MatDialogRef} from "@angular/material";
import {SimpleViewFormService} from "../../services/simple-view-form.service";
@Component({
  selector: "app-simple-view-game-form",
  templateUrl: "./simple-view-game-form.component.html",
  styleUrls: ["./simple-view-game-form.component.css"],
})
export class SimpleViewGameFormComponent {
  private readonly MAX_LENGTH: number = 12;
  private readonly MIN_LENGTH: number = 3;
  private readonly IMAGE_STATUS_LENGTH: number = 4;
  public closeDialog: boolean = false;
  public isSendingForm: boolean = false;
  public imageStatus: boolean[];

  public constructor(private formBuilder: FormBuilder, private service: SimpleViewFormService,
                     public dialogRef: MatDialogRef<SimpleViewGameFormComponent>) {
    this.imageStatus = new Array(this.IMAGE_STATUS_LENGTH).fill(false);
  }
  public simpleViewGameForm: FormGroup = this.formBuilder.group({
      gameName: new FormControl
      ("", Validators.compose([Validators.maxLength(this.MAX_LENGTH), Validators.minLength(this.MIN_LENGTH), Validators.required])),
      originalImage: [null, Validators.required],
      modifiedImage: [null, Validators.required],
    });

  // tslint:disable-next-line:no-any Used to mock the http call
  public processFile(event: any, isOriginal: boolean): void {
      const reader: FileReader = new FileReader();
      if (event.target && event.target) {
        reader.readAsArrayBuffer(event.target.files[0]);
        isOriginal ?
        (reader.onload = () => (this.simpleViewGameForm.patchValue({
          originalImage: new Uint8Array(reader.result as ArrayBuffer),
        }))) :
       (reader.onload = () => (this.simpleViewGameForm.patchValue({
          modifiedImage: new Uint8Array(reader.result as ArrayBuffer),
        })));
      }
  }

  public submitSimpleViewGameForm(): void {
    this.isSendingForm = true;
    this.service.submitSimpleForm(this.simpleViewGameForm.value).then(() => {
      this.isSendingForm = false;
      this.dialogRef.close();
      window.location.reload();
    } ).catch(() => {
      this.imageStatus = this.service.responseHandler();
      this.isSendingForm = false;
    });

  }

}
